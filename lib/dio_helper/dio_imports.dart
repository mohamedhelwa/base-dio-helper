import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:dio/dio.dart';
import 'package:dio_helper/modals/loading_dialog.dart';
import 'package:dio_helper/utils/dio_utils.dart';
import 'package:dio_helper/utils/global_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tf_dio_cache/dio_http_cache.dart';

part 'dio_helper_status.dart';
part 'prev_dio_helper.dart';
